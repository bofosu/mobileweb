package com.mobileweb.fofole.api.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
@Getter
@Setter
public class Model {
    @Id
    private String id = UUID.randomUUID().toString();
    private Date created = new Date();
}
