package com.mobileweb.fofole.api.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Getter
@Setter
public class Concept extends Model {
    private String name;
    private String description;
}
