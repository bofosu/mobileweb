package com.mobileweb.fofole.api.controllers;

import com.mobileweb.fofole.api.exceptions.AlreadyExistException;
import com.mobileweb.fofole.api.exceptions.NoEntityExistException;
import com.mobileweb.fofole.api.models.Category;
import com.mobileweb.fofole.api.services.CategoryService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PutMapping
    public void save(@RequestBody Category category) throws AlreadyExistException {
        categoryService.save(category);
    }

    ///api/categories/dfsakjfsdak
    @PostMapping("/{id}")
    public void update(@RequestBody Category category, @PathVariable String id) throws NoEntityExistException {
        categoryService.update(category, id);
    }

    ///api/asdfa/cat
    @GetMapping("/{id}")
    public Category findById(@PathVariable String id) throws NoEntityExistException {
        return categoryService.findById(id);
    }
}
