package com.mobileweb.fofole.api.services;

import com.mobileweb.fofole.api.models.Product;
import com.mobileweb.fofole.api.repos.ProductRepository;
import org.springframework.stereotype.Service;

@Service
public class ProductService extends GenericService<Product> {

    public ProductService(ProductRepository repository) {
        super(repository);
    }
}
