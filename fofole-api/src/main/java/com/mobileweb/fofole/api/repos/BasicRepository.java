package com.mobileweb.fofole.api.repos;

import com.mobileweb.fofole.api.models.Model;
import org.springframework.data.repository.CrudRepository;

public interface BasicRepository<T extends Model> extends CrudRepository<T, String> {
}
