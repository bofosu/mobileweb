package com.mobileweb.fofole.api.services;

import com.mobileweb.fofole.api.exceptions.AlreadyExistException;
import com.mobileweb.fofole.api.exceptions.NoEntityExistException;
import com.mobileweb.fofole.api.models.Model;
import com.mobileweb.fofole.api.repos.BasicRepository;

import java.util.Optional;

public class GenericService<T extends Model> implements ApiService<T>{

    protected final BasicRepository<T> repository ;

    public GenericService(BasicRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public void save(T type) throws AlreadyExistException {
        Optional<T> optional = repository.findById(type.getId());
        if (optional.isPresent()) {
            throw new AlreadyExistException("The " + type.getClass().getName() + " with id " + type.getId() + " already exist");
        }

        repository.save(type);
    }

    @Override
    public void update(T type, String id) throws NoEntityExistException {
        Optional<T> optional = repository.findById(id);
        if(!optional.isPresent()) {
            throw new NoEntityExistException("The id provided didn't match any type in the database");
        }
        repository.save(type);
    }

    @Override
    public void delete(String id) throws NoEntityExistException {
        Optional<T> optional = repository.findById(id);
        if(!optional.isPresent()) {
            throw new NoEntityExistException("The id provided didn't match any category in the database");
        }

        repository.deleteById(id);
    }

    @Override
    public T findById(String id) throws NoEntityExistException {
        Optional<T> optional = repository.findById(id);
        if(!optional.isPresent()) {
            throw new NoEntityExistException("The id provided didn't match any category in the database");
        }

        return optional.get();
    }
}
