package com.mobileweb.fofole.api.repos;

import com.mobileweb.fofole.api.models.Product;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends BasicRepository<Product> {
}
