package com.mobileweb.fofole.api.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "location")
@Getter
@Setter
public class Location extends Model {

    @Column(name = "digital_address")
    private String digitalAddress;

    private String district;
    private String town;
    private String city;

}
