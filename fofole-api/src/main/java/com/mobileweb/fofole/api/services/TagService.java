package com.mobileweb.fofole.api.services;

import com.mobileweb.fofole.api.models.Tag;
import com.mobileweb.fofole.api.repos.TagRepository;
import org.springframework.stereotype.Service;

@Service
public class TagService extends GenericService<Tag>{

    public TagService(TagRepository repository) {
        super(repository);
    }
}
