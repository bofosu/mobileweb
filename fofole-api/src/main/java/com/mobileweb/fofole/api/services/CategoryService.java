package com.mobileweb.fofole.api.services;

import com.mobileweb.fofole.api.models.Category;
import com.mobileweb.fofole.api.repos.CategoryRepository;
import org.springframework.stereotype.Service;

@Service
public class CategoryService extends GenericService<Category> {

    public CategoryService(CategoryRepository repository) {
        super(repository);
    }
}
