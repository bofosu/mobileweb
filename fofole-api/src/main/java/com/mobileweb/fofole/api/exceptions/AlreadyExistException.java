package com.mobileweb.fofole.api.exceptions;

public class AlreadyExistException extends Exception {
    private static final long serialVersionUID = -1472046627973596776L;

    public AlreadyExistException(String message) {
        super(message);
    }
}
