package com.mobileweb.fofole.api.services;

import com.mobileweb.fofole.api.exceptions.AlreadyExistException;
import com.mobileweb.fofole.api.exceptions.NoEntityExistException;
import com.mobileweb.fofole.api.models.Model;

public interface ApiService<T extends Model> {

    void save(T type) throws AlreadyExistException;

    void update(T type, String id) throws NoEntityExistException;

    void delete(String id) throws NoEntityExistException;

    T findById(String id) throws NoEntityExistException;
}
