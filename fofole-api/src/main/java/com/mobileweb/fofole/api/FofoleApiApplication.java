package com.mobileweb.fofole.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FofoleApiApplication {

  //this is the main method to this app
    public static void main(String[] args) {
        SpringApplication.run(FofoleApiApplication.class, args);
    }
}
