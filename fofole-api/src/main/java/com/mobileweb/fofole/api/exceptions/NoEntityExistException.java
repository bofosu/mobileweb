package com.mobileweb.fofole.api.exceptions;

public class NoEntityExistException extends Exception {
    private static final long serialVersionUID = -1472046627973596776L;

    public NoEntityExistException(String message) {
        super(message);
    }
}
