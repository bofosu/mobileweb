package com.mobileweb.fofole.api.repos;

import com.mobileweb.fofole.api.models.Tag;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends BasicRepository<Tag> {
}
