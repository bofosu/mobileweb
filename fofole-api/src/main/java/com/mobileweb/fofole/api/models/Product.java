package com.mobileweb.fofole.api.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Future;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "product")
@Getter
@Setter
public class Product extends Concept {

    @Column(name = "expiry_date")
    @Future
    private Date expiryDate;

    @ManyToOne
    private Category category;

    @ManyToMany
    @JoinTable(name = "prod_tag",
            joinColumns = @JoinColumn(name = "prod_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<Tag> tags;
    private long rating;
    private double amount;
    private boolean negotiable;

    @ManyToOne
    private User user;

    @ManyToOne
    private Location location;
}
